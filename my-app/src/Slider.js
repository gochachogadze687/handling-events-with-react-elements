import React from 'react';
import './slider.css'; 

const Slider = () => {
  const changeTheme = () => {
    document.body.classList.toggle('dark');
  };

  return (
    <div className="slider-container">
      <label className="switch" htmlFor="checkbox">
        <input type="checkbox" id="checkbox" onChange={changeTheme} />
        <div className="slider round"></div>
      </label>
    </div>
  );
};

export default Slider;
